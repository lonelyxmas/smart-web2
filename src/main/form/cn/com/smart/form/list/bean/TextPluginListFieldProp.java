package cn.com.smart.form.list.bean;

/**
 * 文本框字段属性
 * @author lmq
 */
public class TextPluginListFieldProp extends AbstractListFieldProp {

    private String url;
    
    /**
     * 文本框插件类型
     */
    private String textPluginType;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTextPluginType() {
        return textPluginType;
    }

    public void setTextPluginType(String textPluginType) {
        this.textPluginType = textPluginType;
    }
    
}
