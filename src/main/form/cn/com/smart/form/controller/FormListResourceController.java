package cn.com.smart.form.controller;

import cn.com.smart.bean.SmartResponse;
import cn.com.smart.constant.ResourceConstant;
import cn.com.smart.form.bean.entity.TFormList;
import cn.com.smart.form.service.FormListService;
import cn.com.smart.report.service.impl.ReportButtonService;
import cn.com.smart.report.service.impl.ReportService;
import cn.com.smart.web.bean.RequestPage;
import cn.com.smart.web.bean.entity.TNOPAuth;
import cn.com.smart.web.bean.entity.TNResource;
import cn.com.smart.web.constant.enums.BtnAuthType;
import cn.com.smart.web.constant.enums.BtnPropType;
import cn.com.smart.web.controller.base.BaseController;
import cn.com.smart.web.helper.HttpRequestHelper;
import cn.com.smart.web.service.OPService;
import cn.com.smart.web.service.ResourceService;
import cn.com.smart.web.tag.bean.DelBtn;
import cn.com.smart.web.tag.bean.EditBtn;
import cn.com.smart.web.tag.bean.PageParam;
import cn.com.smart.web.tag.bean.RefreshBtn;
import com.mixsmart.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报表资源管理控制类
 * @author lmq  2017年10月11日
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping("/form/list/resource")
public class FormListResourceController extends BaseController {
    
    private static final String VIEW_DIR = "form/list/resource";
    
    @Autowired
    private OPService opServ;
    @Autowired
    private ResourceService resServ;
    @Autowired
    private FormListService formListServ;

    /**
     * 表单资源管理列表
     * @param request HttpServletRequest请求对象
     * @param page 分页对象
     * @return 返回列表视图
     * @throws Exception
     */
    @RequestMapping("/list")
    public ModelAndView list(HttpServletRequest request, RequestPage page) {
        ModelAndView modelView = new ModelAndView();
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("orgIds", StringUtils.list2Array(getUserInfoFromSession(request).getOrgIds()));
        SmartResponse<Object> smartResp = opServ.getDatas("form_list_resource_mgr_list",params, page.getStartNum(), page.getPageSize());
        params = null;
        String uri = HttpRequestHelper.getCurrentUri(request);
        addBtn = new EditBtn("add","showPage/form_list_resource_add", null, "添加表单list资源", "600");
        editBtn = new EditBtn("edit","showPage/form_list_resource_edit", "resource", "修改表单list资源", "600");
        delBtn = new DelBtn("resource/delete.json", "确定要删除选中的表单list资源吗？",uri,null, null);
        refreshBtn = new RefreshBtn(uri, "resource",null);
        pageParam = new PageParam(uri, null, page.getPage(), page.getPageSize());
        
        ModelMap modelMap = modelView.getModelMap();
        modelMap.put("smartResp", smartResp);
        modelMap.put("addBtn", addBtn);
        modelMap.put("editBtn", editBtn);
        modelMap.put("delBtn", delBtn);
        modelMap.put("refreshBtn", refreshBtn);
        modelMap.put("pageParam", pageParam);
        
        modelView.setViewName(VIEW_DIR+"/list");
        return modelView;
    }
    
    /**
     * 添加表单list资源
     * @param resource 资源实体对象
     * @return 返回添加结果
     */
    @RequestMapping(value="/add",method=RequestMethod.POST)
    @ResponseBody
    public SmartResponse<String> add(TNResource resource) {
        SmartResponse<String> smartResp = new SmartResponse<String>();
        if(null != resource) {
            String formListId = resource.getUri();
            resource.setType(ResourceConstant.FORM_LIST_RESOURCE);
            String uri = "form/list/instance?id="+resource.getUri();
            resource.setUri(uri);
            addBtnAuths(resource, formListId);
            smartResp = resServ.save(resource);
        }
        return smartResp;
    }
    
    /**
     * 编辑表单资源
     * @param resource 资源实体对象
     * @return 返回添加结果
     */
    @RequestMapping(value="/edit",method=RequestMethod.POST)
    @ResponseBody
    public SmartResponse<String> edit(TNResource resource) {
        SmartResponse<String> smartResp = new SmartResponse<String>();
        if(null != resource) {
            String formListId = resource.getUri();
            resource.setType(ResourceConstant.FORM_LIST_RESOURCE);
            String uri = "form/list/instance?id="+resource.getUri();
            resource.setUri(uri);
            addBtnAuths(resource, formListId);
            smartResp = resServ.update(resource);
        }
        return smartResp;
    }

    /**
     * 添加按钮权限
     * @param resource
     * @param formListId
     */
    private void addBtnAuths(TNResource resource, String formListId) {
        List<TNOPAuth> auths = new ArrayList<>(1);
        TNOPAuth auth = new TNOPAuth();
        auth.setValue(BtnAuthType.EXPORT.getValue());
        auths.add(auth);
        TFormList formList = formListServ.find(formListId).getData();
        if(null != formList) {
            Map<String, Object> param = new HashMap<>(1);
            param.put("reportId", formList.getReportId());
            SmartResponse<Object> smartResp = opServ.getDatas("get_auth_btns_by_report_id", param);
            if(OP_SUCCESS.equals(smartResp.getResult())) {
                for(Object obj : smartResp.getDatas()) {
                    Object[] objects = (Object[]) obj;
                    TNOPAuth tmpAuth = new TNOPAuth();
                    tmpAuth.setValue(StringUtils.handleNull(objects[0]));
                    auths.add(tmpAuth);
                }
            }
        }
        resource.setAuths(auths);
    }


}
