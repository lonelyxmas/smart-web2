package cn.com.smart.report.bean.entity;

import cn.com.smart.bean.BaseBeanImpl;
import cn.com.smart.bean.DateBean;
import cn.com.smart.web.constant.enums.BtnPropType;
import cn.com.smart.web.tag.BaseTag;
import com.mixsmart.enums.YesNoType;

import javax.persistence.*;
import java.util.Date;

/**
 * 报表按钮
 */
@Entity
@Table(name = "t_report_button")
public class TReportButton extends BaseBeanImpl implements DateBean {

    private String id;

    private String reportId;

    private String btnId;

    private Integer sortOrder;

    private String url;

    private String btnStyle = BaseTag.BTN_DEFAULT_THEME;

    private String name;

    /**
     * 数据选中类型；详情请查看{@link BtnPropType.SelectType}
     */
    private String selectedType = BtnPropType.SelectType.NONE.getValue();

    /**
     * 是否控制按钮权限；默认授权
     */
    private Integer isAuth = YesNoType.YES.getIndex();

    /**
     * 点击按钮时的打开方式；默认为空
     */
    private String openType = BtnPropType.OpenStyle.NONE.getValue();

    private String dialogTitle;

    private String dialogWidth;

    private String btnIcon;

    private String paramName;

    private String promptMsg;

    private Date createTime;

    private String userId;

    @Id
    @Column(name = "id", length = 50)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "report_id", length = 50, nullable = false)
    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    @Column(name = "btn_id", length = 50, nullable = false)
    public String getBtnId() {
        return btnId;
    }

    public void setBtnId(String btnId) {
        this.btnId = btnId;
    }

    @Column(name = "sort_order")
    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Column(name = "url", length = 255)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "btn_style", length = 100, nullable = false)
    public String getBtnStyle() {
        return btnStyle;
    }

    public void setBtnStyle(String btnStyle) {
        this.btnStyle = btnStyle;
    }

    @Column(name = "name", length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "selected_type", length = 50, nullable = false)
    public String getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(String selectedType) {
        this.selectedType = selectedType;
    }

    @Column(name = "is_auth", nullable = false)
    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    @Column(name = "open_type", length = 100)
    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    @Column(name = "dialog_title", length = 127)
    public String getDialogTitle() {
        return dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    @Column(name = "dialog_width", length = 127)
    public String getDialogWidth() {
        return dialogWidth;
    }

    public void setDialogWidth(String dialogWidth) {
        this.dialogWidth = dialogWidth;
    }

    @Column(name = "btn_icon", length = 100)
    public String getBtnIcon() {
        return btnIcon;
    }

    public void setBtnIcon(String btnIcon) {
        this.btnIcon = btnIcon;
    }

    @Column(name = "param_name", length = 50)
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Column(name = "prompt_msg", length = 255)
    public String getPromptMsg() {
        return promptMsg;
    }

    public void setPromptMsg(String promptMsg) {
        this.promptMsg = promptMsg;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", updatable = false)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Column(name = "user_id", length = 50, nullable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
